//
//  ViewController.m
//  Demo
//
//  Created by yans on 2019/10/11.
//  Copyright © 2019 hzty. All rights reserved.
//

#import "ViewController.h"
#import "BViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.navigationController pushViewController:BViewController.new animated:YES];
    });
}


@end
