//
//  NSObject+KVCExceptionHandler.m
//  STC
//
//  Created by yans on 2018/12/29.
//  Copyright © 2018 hzty. All rights reserved.
//

@implementation NSObject (KVCExceptionHandler)

#if !DEBUG

/**
 增加对 KVC 的异常处理

 @param key
 @return
 */
- (nullable id)valueForUndefinedKey:(NSString *)key
{
    NSLog(@"valueForUndefinedKey exception !!! %@ - > %@", self, key);
    return nil;
}

#endif

@end
