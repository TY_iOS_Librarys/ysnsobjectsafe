//
//  NSDictionary+Safe.m
//  NSObjectSafe
//
//  Created by yans on 2018/4/20.
//  Copyright © 2018年 tencent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Swzzling.h"

#if __has_include("QWLoadable.h")
#import "QWLoadable.h"
#endif

@implementation NSDictionary (Safe)

#if __has_include("QWLoadable.h")
    // auto load
QWLoadableServiceFunctions(nsdictionary_load, {
    [NSDictionary nsdictionary_load];
});
#else
+ (void)load
{
    [NSDictionary nsdictionary_load];
}

#endif


+ (void)nsdictionary_load
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        ss_swizzling_exchangeMethod(self, @selector(objectForKey:), @selector(safe_objectForKey:));
        ss_swizzling_exchangeMethod(self, @selector(objectForKeyedSubscript:), @selector(safe_objectForKeyedSubscript:));
        ss_swizzling_exchangeMethod(self, @selector(initWithObjects:forKeys:count:), @selector(safe_initWithObjects:forKeys:count:));
        ss_swizzling_exchangeMethod(self, @selector(dictionaryWithObjects:forKeys:count:), @selector(safe_dictionaryWithObjects:forKeys:count:));
        ss_swizzling_exchangeMethod(objc_getClass("__NSPlaceholderDictionary"), @selector(initWithObjects:forKeys:count:), @selector(safe_placeholderDictionary_initWithObjects:forKeys:count:));
        
    });
}

#pragma mark -  - (id)objectForKey:
- (id)safe_objectForKey:(nonnull id)key
{
    if (!key || key == (id) [NSNull null]) {
        return nil;
    }
    
    return [self safe_objectForKey:key];
}

#pragma mark -  - (id)objectForKeyedSubscript:
- (id)safe_objectForKeyedSubscript:(nonnull id)key
{
    if (!key || key == (id) [NSNull null]) {
        return nil;
    }
    
    return [self safe_objectForKeyedSubscript:key];
}

#pragma mark - initWithObjects:forKeys:count:
- (instancetype)safe_initWithObjects:(const id [])objects forKeys:(const id<NSCopying> [])keys count:(NSUInteger)cnt {
    id safeObjects[cnt];
    id safeKeys[cnt];
    NSUInteger j = 0;
    for (NSUInteger i = 0; i < cnt; i++) {
        id key = keys[i];
        id obj = objects[i];
        if (!key || !obj) {
            continue;
        }
        
//        if (key == (id) [NSNull null]) {
//            continue;
//        }
        
        safeKeys[j] = key;
        safeObjects[j] = obj;
        j++;
    }
    return [self safe_initWithObjects:safeObjects forKeys:safeKeys count:j];
}

- (instancetype)safe_placeholderDictionary_initWithObjects:(const id [])objects forKeys:(const id<NSCopying> [])keys count:(NSUInteger)cnt {
    id safeObjects[cnt];
    id safeKeys[cnt];
    NSUInteger j = 0;
    for (NSUInteger i = 0; i < cnt; i++) {
        id key = keys[i];
        id obj = objects[i];
        if (!key || !obj) {
            continue;
        }
        
//        if (key == (id) [NSNull null]) {
//            continue;
//        }
        
        safeKeys[j] = key;
        safeObjects[j] = obj;
        j++;
    }
    return [self safe_placeholderDictionary_initWithObjects:safeObjects forKeys:safeKeys count:j];
}

#pragma mark - dictionaryWithObjects:forKeys:count:
+ (instancetype)safe_dictionaryWithObjects:(const id [])objects forKeys:(const id<NSCopying> [])keys count:(NSUInteger)cnt {
    id safeObjects[cnt];
    id safeKeys[cnt];
    NSUInteger j = 0;
    for (NSUInteger i = 0; i < cnt; i++) {
        id key = keys[i];
        id obj = objects[i];
        if (!key || !obj) {
            continue;
        }
        
//        if (key == (id) [NSNull null]) {
//            continue;
//        }
        
        safeKeys[j] = key;
        safeObjects[j] = obj;
        j++;
    }
    return [self safe_dictionaryWithObjects:safeObjects forKeys:safeKeys count:j];
}

@end

#pragma mark -

@implementation NSMutableDictionary (Safe)

+ (void)load
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{

        ss_swizzling_exchangeMethod(objc_getClass("__NSDictionaryM"), @selector(setValue:forKey:), @selector(safe_setValue:forKey:));
        ss_swizzling_exchangeMethod(objc_getClass("__NSDictionaryM"), @selector(setObject:forKey:), @selector(safe_setObject:forKey:));
        ss_swizzling_exchangeMethod(objc_getClass("__NSDictionaryM"), @selector(removeObjectForKey:), @selector(safe_removeObjectForKey:));
        ss_swizzling_exchangeMethod(objc_getClass("__NSDictionaryM"), @selector(setObject:forKeyedSubscript:), @selector(safe_setObject:forKeyedSubscript:));

//        ss_swizzling_exchangeMethod(self, @selector(setValue:forKey:), @selector(safe_setValue:forKey:));
//        ss_swizzling_exchangeMethod(self, @selector(setObject:forKey:), @selector(safe_setObject:forKey:));
//        ss_swizzling_exchangeMethod(self, @selector(removeObjectForKey:), @selector(safe_removeObjectForKey:));
//        ss_swizzling_exchangeMethod(self, @selector(setObject:forKeyedSubscript:), @selector(safe_setObject:forKeyedSubscript:));

    });
}

#pragma mark - setValue:forKey:
- (void)safe_setValue:(id)value forKey:(NSString *)key
{
    if (!key) {
        return;
    }
    
    [self safe_setValue:value forKey:key];
}

#pragma mark - setObject:forKey:
- (void)safe_setObject:(id)object forKey:(id<NSCopying>)key
{
    if (!key || !object) {
        return;
    }
//    if (key == (id) [NSNull null]) {
//        return;
//    }

    [self safe_setObject:object forKey:key];
}

#pragma mark - removeObjectForKey:
- (void)safe_removeObjectForKey:(id)key
{
    if (!key) {
        return;
    }
    
//    if (key == (id) [NSNull null]) {
//        return;
//    }
    
    [self safe_removeObjectForKey:key];
}

#pragma mark - setObject:forKeyedSubscript:
- (void)safe_setObject:(id)obj forKeyedSubscript:(id<NSCopying>)key
{
    if (!key || !obj) {
        return;
    }
//    if (key == (id) [NSNull null]) {
//        return;
//    }
    
    [self safe_setObject:obj forKeyedSubscript:key];
}



@end
