//
//  UITextField+Fix_iOS11_2.m
//  STC
//
//  Created by yans on 2018/1/8.
//  Copyright © 2018年 hzty. All rights reserved.
//

//typedef void (^DeallocBlock)();
//@interface UITextFieldDealloc : NSObject
//@property (nonatomic, copy) DeallocBlock block;
//- (instancetype)initWithBlock:(DeallocBlock)block;
//@end
//
//@implementation UITextFieldDealloc
//
//- (instancetype)initWithBlock:(DeallocBlock)block
//{
//    self = [super init];
//    if (self) {
//        self.block = block;
//    }
//    return self;
//}
//- (void)dealloc {
//    self.block ? self.block() : nil;
//}
//@end

#import "Swzzling.h"

#if __has_include("QWLoadable.h")
#import "QWLoadable.h"
#endif

/**
 修复iOS11.2上内存泄露问题
 retain cycle is:
 "_UITextFieldContentView._provider -> UITextField -> _textContentView -> _UITextFieldContentView "
 */
@interface UITextField (Fix_iOS11_2)

@property (nonatomic, weak) id    originalProvider;

@end

@implementation UITextField (Fix_iOS11_2)

- (void)setOriginalProvider:(id)originalProvider
{
    objc_setAssociatedObject(self, @selector(originalProvider), originalProvider, OBJC_ASSOCIATION_ASSIGN);

//    UITextFieldDealloc *db = [self getDeallocBlock];
//    // 这里关联的key必须唯一，如果使用_cmd，对一个对象多次关联的时候，前面的对象关联会失效。
//    objc_setAssociatedObject(originalProvider, (__bridge const void *)(db.block), db, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
//    objc_setAssociatedObject(self, @selector(originalProvider), originalProvider, OBJC_ASSOCIATION_ASSIGN);
}

//- (UITextFieldDealloc *)getDeallocBlock
//{
//    __weak typeof(self) wkself = self;//这里补加句 __weak 不然block中又引用了一次 导致无法释放问题
//    return [[UITextFieldDealloc alloc] initWithBlock:^{
//        objc_setAssociatedObject(wkself, @selector(originalProvider), nil, OBJC_ASSOCIATION_ASSIGN);
//    }];
//}

- (id)originalProvider
{
    return objc_getAssociatedObject(self, _cmd);
}

//- (void)ys_dealloc
//{
//    [self ys_dealloc];
//
//    {//这个属性 还是干掉试试
//        objc_setAssociatedObject(self, @selector(originalProvider), nil, OBJC_ASSOCIATION_ASSIGN);
//    }
//}

- (void)ys_didMoveToWindow
{
    [self ys_didMoveToWindow];//调用 原始方法
    
    if (@available(iOS 11.2, *)) {
        NSString *keyPath = @"textContentView.provider";
        @try {
            if (self.window) {
                id provider = [self valueForKeyPath:keyPath];
                if (!provider && self.originalProvider) {
                    [self setValue:self.originalProvider forKeyPath:keyPath];
                }
            } else {
                self.originalProvider = [self valueForKeyPath:keyPath];
                [self setValue:nil forKeyPath:keyPath];
            }
        } @catch (NSException *exception) {
            NSLog(@"%s exception : %@",__func__, exception);
        }
    }
}

+ (void)fix_ios12_load
{
    if (@available(iOS 11.2, *)) {
        static dispatch_once_t once;
        dispatch_once( &once, ^{
            ss_swizzling_exchangeMethod(self, @selector(didMoveToWindow), @selector(ys_didMoveToWindow));

            // [YSSwzzling exchangeInstanceMethod:@selector(didMoveToWindow) with:@selector(ys_didMoveToWindow) class:self];

            // [YSSwzzling exchangeInstanceMethod:@selector(dealloc) with:@selector(ys_dealloc) class:self];
        });
    }
}


#if __has_include("QWLoadable.h")
    // auto load
QWLoadableLazyFunctions(fix_ios12_load, {
    [UITextField fix_ios12_load];
});
#else
+ (void)load
{
    [self fix_ios12_load];
}

#endif


@end

