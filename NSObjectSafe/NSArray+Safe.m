//
//  NSArray+Safe.m
//  TestSafeDic
//
//  Created by yans on 2018/4/20.
//  Copyright © 2018年 hzty. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Swzzling.h"

#if __has_include("QWLoadable.h")
#import "QWLoadable.h"
#endif

@implementation NSArray (Safe)

#if __has_include("QWLoadable.h")
    // auto load
QWLoadableServiceFunctions(nsarray_load, {
    [NSArray nsarray_load];
});
#else
+ (void)load
{
    [self nsarray_load];
}

#endif


+ (void)nsarray_load
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        ss_swizzling_exchangeMethod(objc_getClass("__NSArray0"), @selector(objectAtIndex:), @selector(safe_array0_objectAtIndex:));
        ss_swizzling_exchangeMethod(objc_getClass("__NSArrayI"), @selector(objectAtIndex:), @selector(safe_arrayI_objectAtIndex:));
        ss_swizzling_exchangeMethod(objc_getClass("__NSArrayM"), @selector(objectAtIndex:), @selector(safe_arrayM_objectAtIndex:));
        ss_swizzling_exchangeMethod(objc_getClass("__NSSingleObjectArrayI"), @selector(objectAtIndex:), @selector(safe_singleObjectArrayI_objectAtIndex:));
        
        ss_swizzling_exchangeMethod(objc_getClass("__NSArray0"), @selector(objectAtIndexedSubscript:), @selector(safe_array0_objectAtIndexedSubscript:));
        ss_swizzling_exchangeMethod(objc_getClass("__NSArrayI"), @selector(objectAtIndexedSubscript:), @selector(safe_arrayI_objectAtIndexedSubscript:));
        ss_swizzling_exchangeMethod(objc_getClass("__NSArrayM"), @selector(objectAtIndexedSubscript:), @selector(safe_arrayM_objectAtIndexedSubscript:));
        ss_swizzling_exchangeMethod(objc_getClass("__NSSingleObjectArrayI"), @selector(objectAtIndexedSubscript:), @selector(safe_singleObjectArrayI_objectAtIndexedSubscript:));
        
        ss_swizzling_exchangeMethod(objc_getClass("__NSPlaceholderArray"), @selector(initWithObjects:count:), @selector(safe_initWithObjects:count:));
        
        ss_swizzling_exchangeMethod(objc_getClass("__NSArrayM"), @selector(addObject:), @selector(safe_arrayM_addObject:));
        ss_swizzling_exchangeMethod(objc_getClass("__NSArrayM"), @selector(setObject:atIndexedSubscript:), @selector(safe_arrayM_setObject:atIndexedSubscript:));
        ss_swizzling_exchangeMethod(objc_getClass("__NSArrayM"), @selector(insertObject:atIndex:), @selector(safe_arrayM_insertObject:atIndex:));
        ss_swizzling_exchangeMethod(objc_getClass("__NSArrayM"), @selector(removeObjectAtIndex:), @selector(safe_arrayM_removeObjectAtIndex:));

        ss_swizzling_exchangeMethod(objc_getClass("__NSArrayM"), @selector(removeObjectsInRange:), @selector(safe_arrayM_removeObjectsInRange:));
    });
}

#pragma mark -  - (id)objectAtIndex:
- (id)safe_array0_objectAtIndex:(NSUInteger)index{
    return nil;
}

- (id)safe_arrayI_objectAtIndex:(NSUInteger)index{
    if (index >= self.count) {
        return nil;
    }

    return [self safe_arrayI_objectAtIndex:index];
}

- (id)safe_arrayM_objectAtIndex:(NSUInteger)index{
    if (index >= self.count) {
        return nil;
    }
    
    return [self safe_arrayM_objectAtIndex:index];
}

- (id)safe_singleObjectArrayI_objectAtIndex:(NSUInteger)index{
    if (index >= self.count) {
        return nil;
    }
    
    return [self safe_singleObjectArrayI_objectAtIndex:index];
}

#pragma mark -  - (id)objectAtIndexedSubscript:
- (id)safe_array0_objectAtIndexedSubscript:(NSUInteger)index{
    return nil;
}

- (id)safe_arrayI_objectAtIndexedSubscript:(NSUInteger)index{
    if (index >= self.count) {
        return nil;
    }

    return [self safe_arrayI_objectAtIndex:index];
}

- (id)safe_arrayM_objectAtIndexedSubscript:(NSUInteger)index{
    if (index >= self.count) {
        return nil;
    }

    return [self safe_arrayM_objectAtIndex:index];
}

- (id)safe_singleObjectArrayI_objectAtIndexedSubscript:(NSUInteger)index{
    if (index >= self.count) {
        return nil;
    }

    return [self safe_singleObjectArrayI_objectAtIndexedSubscript:index];
}

- (instancetype)safe_initWithObjects:(id  _Nonnull const [])objects count:(NSUInteger)cnt
{
    NSUInteger newCount = 0;
    for (NSUInteger i = 0; i < cnt; i++) {
        if (!objects[i]) {
            break;
        }
        newCount++;
    }
    return [self safe_initWithObjects:objects count:newCount];
}

- (void)safe_arrayM_addObject:(id)object
{
    if (!object) {
        return;
    }
 
    [self safe_arrayM_addObject:object];
}

- (void)safe_arrayM_setObject:(nonnull id)object atIndexedSubscript:(NSUInteger)index
{
    if(object == nil){
        return;
    }
    if(index > self.count){
        index = self.count;
    }
    return [self safe_arrayM_setObject:object atIndexedSubscript:index];
}

- (void)safe_arrayM_insertObject:(id)object atIndex:(NSUInteger)index
{
    if(object == nil){
        return;
    }
    
    if(index > self.count){
        index = self.count;
    }
    return [self safe_arrayM_insertObject:object atIndex:index];
}

- (void)safe_arrayM_removeObjectAtIndex:(NSUInteger)index
{
    if (index >= self.count) {
        return;
    }
    
    [self safe_arrayM_removeObjectAtIndex:index];
}

- (void)safe_arrayM_removeObjectsInRange:(NSRange)range
{
    if (range.location + range.length > self.count) {
        return;
    }
    
    [self safe_arrayM_removeObjectsInRange:range];
}

@end

