#
# Be sure to run `pod lib lint YSTableView.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'YSNSObjectSafe'
  s.version          = '1.1.0'
  s.summary          = 'A short description of NSObjectSafe.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://github.com/478356182@qq.com/NSObjectSafe'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { '478356182@qq.com' => '478356182@qq.com' }
  s.source           = { :git => 'https://bitbucket.org/TY_iOS_Librarys/ysnsobjectsafe.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'
  s.requires_arc = false
  s.ios.deployment_target = '7.0'

  s.source_files = 'NSObjectSafe/*.{h,m}'
#  s.resource  = 'NSObjectSafe/_loading/*.png'

  # s.resource_bundles = {
  #   'NSObjectSafe' => ['NSObjectSafe/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
   s.frameworks = 'Foundation', 'UIKit'
   s.dependency 'NullSafe'

end
